package cn.spream.jstudy.activemq.producer.service;

import org.springframework.jms.core.JmsTemplate;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-8-14
 * Time: 下午5:16
 * To change this template use File | Settings | File Templates.
 */
public class MqMessageSender {

    private JmsTemplate jmsTemplate;

    public void send(final String text) {
        jmsTemplate.convertAndSend(text);
    }

    public JmsTemplate getJmsTemplate() {
        return jmsTemplate;
    }

    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

}
