package cn.spream.jstudy.designpattern.bridge;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午3:46
 * To change this template use File | Settings | File Templates.
 */
public class Woman extends Person {

    @Override
    public void drive() {
        System.out.println("女人开车");
        getCar().run();
    }
}
