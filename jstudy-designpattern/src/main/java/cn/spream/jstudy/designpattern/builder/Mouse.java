package cn.spream.jstudy.designpattern.builder;

/**
 * 鼠标
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午3:42
 * To change this template use File | Settings | File Templates.
 */
public interface Mouse {

    public void setName(String name);

    public String getName();

}
