package cn.spream.jstudy.designpattern.observer;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午4:42
 * To change this template use File | Settings | File Templates.
 */
public class ConcreteWatcher implements Watcher {

    private String name;

    public ConcreteWatcher(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void update(String str) {
        System.out.println(name + "收到通知：" + str);
    }
}
