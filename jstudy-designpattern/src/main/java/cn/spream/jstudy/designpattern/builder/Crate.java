package cn.spream.jstudy.designpattern.builder;

/**
 * 机箱
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午3:41
 * To change this template use File | Settings | File Templates.
 */
public interface Crate {

    public void setName(String name);

    public String getName();

}
