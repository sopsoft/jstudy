package cn.spream.jstudy.designpattern.prototype;

import java.io.Serializable;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-7
 * Time: 下午4:30
 * To change this template use File | Settings | File Templates.
 */
public class User implements Serializable {

    private long id = new Date().getTime();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                '}';
    }
}
