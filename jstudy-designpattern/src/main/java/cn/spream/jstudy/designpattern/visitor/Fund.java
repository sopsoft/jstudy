package cn.spream.jstudy.designpattern.visitor;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午5:01
 * To change this template use File | Settings | File Templates.
 */
public class Fund implements Service {

    @Override
    public void accept(Visitor visitor) {
        visitor.process(this);
    }

}
