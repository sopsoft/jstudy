package cn.spream.jstudy.designpattern.adapter;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 上午10:18
 * To change this template use File | Settings | File Templates.
 */
public interface Log {

    public void debug(String debug);

    public void info(String info);

    public void warn(String warn);

    public void error(String error);

    public void fatal(String fatal);

}
