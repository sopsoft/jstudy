package cn.spream.jstudy.designpattern.memento;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午3:10
 * To change this template use File | Settings | File Templates.
 */
public class TestMemento {

    @Test
    public void test(){
        Original original = new Original("aaa");
        System.out.println("初始化：" + original.getValue());
        Memento memento = original.createMemento();
        original.setValue("bbb");
        System.out.println("修改后：" + original.getValue());
        Storage storage = new Storage(memento);
        original.restoreMemento(storage.getMemento());
        System.out.println("恢复化：" + original.getValue());
    }

}
