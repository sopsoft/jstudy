package cn.spream.jstudy.designpattern.adapter;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 上午10:37
 * To change this template use File | Settings | File Templates.
 */
public class TestAdapter {

    @Test
    public void testClassAdapter() {
        ClassAdapterLog classAdapterLog = new ClassAdapterLog();
        classAdapterLog.debug("debug");
        classAdapterLog.info("info");
        classAdapterLog.warn("warn");
        classAdapterLog.error("error");
        classAdapterLog.fatal("fatal");
    }

    @Test
    public void testObjectAdapter() {
        ObjectAdapterLog objectAdapterLog = new ObjectAdapterLog(new Log4j());
        objectAdapterLog.debug("debug");
        objectAdapterLog.info("info");
        objectAdapterLog.warn("warn");
        objectAdapterLog.error("error");
        objectAdapterLog.fatal("fatal");
    }

    @Test
    public void testInterfaceAdapterLog() {
        InterfaceAdapterLog interfaceAdapterLog = new InterfaceAdapterLog();
        interfaceAdapterLog.debug("debug");
        interfaceAdapterLog.info("info");
        interfaceAdapterLog.warn("warn");
        interfaceAdapterLog.error("error");
        interfaceAdapterLog.fatal("fatal");
    }

}
