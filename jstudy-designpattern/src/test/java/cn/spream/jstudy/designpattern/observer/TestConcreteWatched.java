package cn.spream.jstudy.designpattern.observer;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-15
 * Time: 下午4:51
 * To change this template use File | Settings | File Templates.
 */
public class TestConcreteWatched {

    @Test
    public void test(){
        ConcreteWatched concreteWatched = new ConcreteWatched();
        Watcher watcher1 = new ConcreteWatcher("watcher1");
        Watcher watcher2 = new ConcreteWatcher("watcher2");
        Watcher watcher3 = new ConcreteWatcher("watcher3");
        concreteWatched.addWatcher(watcher1);
        concreteWatched.addWatcher(watcher2);
        concreteWatched.addWatcher(watcher3);
        concreteWatched.notifyWatchers("hello");
        concreteWatched.removeWatcher(watcher2);
        concreteWatched.notifyWatchers("observer");
    }

}
