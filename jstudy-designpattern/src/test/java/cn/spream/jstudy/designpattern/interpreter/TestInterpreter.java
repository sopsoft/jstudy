package cn.spream.jstudy.designpattern.interpreter;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15/1/18
 * Time: 下午3:57
 * To change this template use File | Settings | File Templates.
 */
public class TestInterpreter {

    @Test
    public void test(){
        Context context = new Context(9, 6);
        Expression plus = new Plus();
        Expression minus = new Minus();
        System.out.println(plus.interpret(context));
        System.out.println(minus.interpret(context));
    }

}
