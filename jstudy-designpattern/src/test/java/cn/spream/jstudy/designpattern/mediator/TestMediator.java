package cn.spream.jstudy.designpattern.mediator;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15/1/18
 * Time: 下午3:38
 * To change this template use File | Settings | File Templates.
 */
public class TestMediator {

    @Test
    public void test(){
        Colleague colleagueA = new ColleagueA();
        Colleague colleagueB = new ColleagueB();
        Mediator mediator = new ConcreteMediator(colleagueA, colleagueB);

        //设置a的值通过mediator影响b的值
        colleagueA.setNumber(1000, mediator);
        System.out.println(colleagueA.getNumber());
        System.out.println(colleagueB.getNumber());

        //设置b的值通过mediator影响a的值
        colleagueB.setNumber(1000, mediator);
        System.out.println(colleagueB.getNumber());
        System.out.println(colleagueA.getNumber());
    }

}
