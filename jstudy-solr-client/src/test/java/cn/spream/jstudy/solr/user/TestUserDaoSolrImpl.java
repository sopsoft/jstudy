package cn.spream.jstudy.solr.user;

import org.apache.solr.client.solrj.SolrServerException;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import cn.spream.jstudy.solr.dao.user.UserDao;
import cn.spream.jstudy.solr.domain.user.User;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-3-20
 * Time: 下午5:16
 * To change this template use File | Settings | File Templates.
 */
public class TestUserDaoSolrImpl {

    private UserDao userDao;

    {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        userDao = (UserDao) context.getBean("userDao");
    }

    @Test
    public void testAdd() throws IOException, SolrServerException {
        User user = new User();
        user.setId(1);
        user.setName("张三");
        user.setSex(1);
        user.setAge(22);
        user.setAddress("北京朝阳区");
        user.setMobile("1388888888");
        userDao.add(user);
    }

    @Test
    public void testGet() throws SolrServerException {
        User user = userDao.getById(1);
        System.out.println(user);
    }

}
